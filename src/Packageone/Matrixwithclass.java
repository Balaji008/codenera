package Packageone;
import java.util.*;

class Matrix1
{
	Scanner sc=new Scanner(System.in);
	int a[][]=new int[3][3];
	int i,j;
	
	public void input()
	{
		System.out.println("Enter Elements of matrix ");
		
		for(i=0; i<a.length; i++)
		{
			for(j=0; j<a[0].length; j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
	}
	
	public void disp()
	{
		System.out.println("Printing Matrix");
		
		for(i=0; i<a.length; i++)
		{
			for(j=0; j<a[0].length; j++)
			{
				System.out.print(a[i][j]+" ");
			}
			System.out.println();
		}
	}
}

public class Matrixwithclass {

	public static void main(String[] args) {
		Matrix1 obj=new Matrix1();
		
		obj.input();
		obj.disp();

	}

}
