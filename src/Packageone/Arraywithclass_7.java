package Packageone;
import java.util.*;

class Array7
{
	Scanner sc=new Scanner(System.in);
	int a[]=new int[10];
	int i,j;
	
	public void input()
	{
		System.out.println("Enter Elements of Array");
		
		for(i=0; i<a.length; i++)
		{
			a[i]=sc.nextInt();
		}
	}
	
	public void ascending()
	{
		int temp;
		
		for(i=0; i<a.length; i++)
		{
			for(j=i+1; j<a.length; j++)
			{
				if(a[i]>a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
			
		}
		
		System.out.print("Array in Ascending Order ");
		
		for(i=0; i<a.length; i++)
		{
			System.out.print(a[i]+" ");
		}
	}
	
	public void decending()
	{
		int temp;
		
		for(i=0; i<a.length; i++)
		{
			for(j=i+1; j<a.length; j++)
			{
				if(a[i]<a[j])
				{
					temp=a[i];
					a[i]=a[j];
					a[j]=temp;
				}
			}
			
		}
		
		System.out.print("\nArray in Decending Order ");
		
		for(i=0; i<a.length; i++)
		{
			System.out.print(a[i]+" ");
		}
	}
}

public class Arraywithclass_7 {

	public static void main(String[] args) {
		
		Array7 obj=new Array7();
		
		obj.input();
		obj.ascending();
		obj.decending();
		// TODO Auto-generated method stub

	}

}
