package Packageone;
import java.util.Scanner;

public class fibonacci {
	int d;
	
	Scanner sc=new Scanner(System.in);
	
	public void input()
	{
		System.out.println("Enter No. of Terms");
		d=sc.nextInt();
	}
	
	public void output()
	{
		int a=0,b=1,c,i;
		
		for(i=2; i<=d; i++)
		{
			c=a+b;
			System.out.print(c+" ");
			a=b;
			b=c;
		}
	}

	public static void main(String[] args)
	{
		fibonacci cl=new fibonacci();
		cl.input();
		cl.output();

	}

}
